#include "Food.h"

#pragma region Object
Food::Food() {
	position = generateRandomPosition();
}
Food::~Food() {
	UnloadTexture(texture);
}
void Food::loadTexture() {
	Image image = LoadImage("Graphics/food.png");
	texture = LoadTextureFromImage(image);
	UnloadImage(image);
}
#pragma endregion

#pragma region Public
void Food::draw() {
	DrawTexture(
		texture,
		position.x * SnakeConstants::CELL_SIZE,
		position.y * SnakeConstants::CELL_SIZE,
		WHITE
	);
}
void Food::findNewPosition(std::deque<Vector2Int> snakeBody) {
	Vector2Int newPosition;
	while (true) {
		newPosition = generateRandomPosition();
		if (Vector2IntEquals(position, newPosition)) {
			std::cout << "CATCH: Food wanted to spawn on the same position!" << std::endl;
			continue;
		}			
		for (int i = 0; i < snakeBody.size(); i++) {
			if (Vector2IntEquals(snakeBody[i], newPosition)) {
				std::cout << "CATCH: Food wanted to spawn on the snake!" << std::endl;
				findNewPosition(snakeBody);
				return;
			}
		}
		break;
	}
	position = newPosition;
}
Vector2Int Food::getPosition() {
	return position;
}
#pragma endregion

#pragma region Private
Vector2Int Food::generateRandomPosition() {
	int cellsX = SnakeConstants::WINDOW_WIDTH / SnakeConstants::CELL_SIZE;
	int cellsY = SnakeConstants::WINDOW_HEIGHT / SnakeConstants::CELL_SIZE;
	int x = GetRandomValue(SnakeConstants::MARGIN.x, cellsX - (SnakeConstants::MARGIN.z + 1));
	int y = GetRandomValue(SnakeConstants::MARGIN.y, cellsY - (SnakeConstants::MARGIN.w + 1));
	return Vector2Int{ x,y };
}
#pragma endregion