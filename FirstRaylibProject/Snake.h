#pragma once
#include <iostream>
#include <raylib.h>
#include <raymath.h>
#include <deque>

#include "Food.h"

class Snake {
public:
	Snake();
	void unloadSounds();
	void loadSounds();

	void draw();
	void update(Food *food);
	void updateDirection(Vector2Int* direction);
	double getSpeed();
	int getFood();
private:
	Vector2Int direction;
	std::deque<Vector2Int> body;
	int foodCounter = 0;
	bool pause;

	Sound eatSound;
	Sound wallSound;
	Sound gameOverSound;

	bool checkFoodCollision(Food* food);
	void checkEdgeCollision();
	void checkTailCollision(Food* food);
	void reset();
};

