#pragma once
#include <raylib.h>

namespace SnakeConstants {
	extern const double SNAKE_SPEED;
	extern const double BOOST_PER_FOOD;
	extern const int CELL_SIZE;
	extern const int WINDOW_WIDTH;
	extern const int WINDOW_HEIGHT;
	extern const int FONT_SIZE;

	extern const Vector4 MARGIN;

	extern const Color GREEN_COLOR;
	extern const Color DARKGREEN_COLOR;
}

struct Vector2Int {
	int x;
	int y;
};
bool Vector2IntEquals(Vector2Int vector1, Vector2Int vector2);
Vector2Int Vector2IntAdd(Vector2Int vector1, Vector2Int vector2);