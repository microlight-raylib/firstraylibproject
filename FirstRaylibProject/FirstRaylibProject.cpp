#include <iostream>
#include <raylib.h>
#include <raymath.h>

#include "Food.h"
#include "Snake.h"
#include "Constants.h"

Snake snake;
Food food;
Vector2Int newDirection = {1,0};
double lastUpdateTime = 0;

bool makeMove() {
    if (GetTime() - lastUpdateTime >= 1 / snake.getSpeed()) {
        lastUpdateTime = GetTime();
        return true;
    }
    return false;
}
void updateInput() {
    if (IsKeyPressed(KEY_UP)) {
        newDirection = { 0,-1 };
    }
    else if (IsKeyPressed(KEY_DOWN)) {
        newDirection = { 0,1 };
    }
    else if (IsKeyPressed(KEY_RIGHT)) {
        newDirection = { 1,0 };
    }
    else if (IsKeyPressed(KEY_LEFT)) {
        newDirection = { -1,0 };
    }
}

int main() {
    InitWindow(
        SnakeConstants::WINDOW_WIDTH, 
        SnakeConstants::WINDOW_HEIGHT, 
        "Retro snake");
    InitAudioDevice();
    SetTargetFPS(60);
    snake = Snake();
    snake.loadSounds();
    food = Food();
    food.loadTexture();

    while (WindowShouldClose() == false) {
        // Update
        updateInput();
        if (makeMove()) {
            snake.updateDirection(&newDirection);
            snake.update(&food);
        }

        // Draw
        BeginDrawing();
        ClearBackground(SnakeConstants::GREEN_COLOR);
        // Draw title text
        DrawText(
            "Microlight Snake",
            (SnakeConstants::MARGIN.x - 1) * SnakeConstants::CELL_SIZE + 10,
            (SnakeConstants::MARGIN.y - 1) * SnakeConstants::CELL_SIZE - SnakeConstants::FONT_SIZE,
            SnakeConstants::FONT_SIZE,
            SnakeConstants::DARKGREEN_COLOR);
        // Draw score
        int stringWidth = MeasureText(TextFormat("%i", snake.getFood()), SnakeConstants::FONT_SIZE);
        DrawText(
            TextFormat("%i", snake.getFood()),
            SnakeConstants::WINDOW_WIDTH - (SnakeConstants::MARGIN.x + SnakeConstants::MARGIN.z - 2) * SnakeConstants::CELL_SIZE - 10.0f - stringWidth,
            (SnakeConstants::MARGIN.y - 1)* SnakeConstants::CELL_SIZE - SnakeConstants::FONT_SIZE,
            SnakeConstants::FONT_SIZE,
            SnakeConstants::DARKGREEN_COLOR);
        // Draw play field edges
        DrawRectangleLinesEx(
            Rectangle{
                (SnakeConstants::MARGIN.x - 1) * SnakeConstants::CELL_SIZE + 5.0f,
                (SnakeConstants::MARGIN.y - 1) * SnakeConstants::CELL_SIZE + 5.0f,
                SnakeConstants::WINDOW_WIDTH - (SnakeConstants::MARGIN.x + SnakeConstants::MARGIN.z - 2) * SnakeConstants::CELL_SIZE - 10.0f,
                SnakeConstants::WINDOW_HEIGHT - (SnakeConstants::MARGIN.y + SnakeConstants::MARGIN.w - 2) * SnakeConstants::CELL_SIZE - 10.0f
            },
            15,
            SnakeConstants::DARKGREEN_COLOR
        );
        // Draw game
        food.draw();
        snake.draw();
        EndDrawing();
    }

    snake.unloadSounds();
    CloseAudioDevice();
    CloseWindow();
    return 0;
}