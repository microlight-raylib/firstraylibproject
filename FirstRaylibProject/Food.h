#pragma once
#include <raylib.h>
#include <raymath.h>
#include <deque>
#include <iostream>

#include "Constants.h"

class Food {
public:
	Food();
	~Food();

	void loadTexture();
	void draw();
	void findNewPosition(std::deque<Vector2Int> snakeBody);
	Vector2Int getPosition();
private:
	Vector2Int position;
	Texture2D texture;

	Vector2Int generateRandomPosition();
};