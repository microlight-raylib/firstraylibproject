#include "Snake.h"
#include "Constants.h"

#pragma region Object
Snake::Snake() {
	reset();
}
void Snake::unloadSounds() {
	UnloadSound(eatSound);
	UnloadSound(wallSound);
	UnloadSound(gameOverSound);
}
void Snake::loadSounds() {
	eatSound = LoadSound("Sounds/eat.wav");
	wallSound = LoadSound("Sounds/wall.wav");
	gameOverSound = LoadSound("Sounds/gameOver.wav");
}
#pragma endregion

#pragma region Public
void Snake::draw() {
	for (int i = 0; i < body.size(); i++) {
		DrawRectangleRounded(
			Rectangle{
				(float)body[i].x * SnakeConstants::CELL_SIZE,
				(float)body[i].y * SnakeConstants::CELL_SIZE,
				(float)SnakeConstants::CELL_SIZE,
				(float)SnakeConstants::CELL_SIZE,
			},
			0.5f,
			2,
			SnakeConstants::DARKGREEN_COLOR
			);
	}
}
void Snake::update(Food *food) {
	if (pause) return;
	body.push_front(Vector2IntAdd(body[0], direction));

	// Edge collision
	checkEdgeCollision();

	// Food collision
	if (checkFoodCollision(food)) {
		foodCounter++;
		PlaySound(eatSound);
		std::cout << "INFO: New speed is " << getSpeed() << std::endl;
	}
	else {
		body.pop_back();
	}	

	// Tail coillision
	checkTailCollision(food);
}
void Snake::updateDirection(Vector2Int* newDirection) {	
	if (newDirection->x == -direction.x) return;
	if (newDirection->y == -direction.y) return;
	direction = { newDirection->x, newDirection->y };
	std::cout << "INFO: New direction " << newDirection->x << "," << newDirection->y << std::endl;
	*newDirection = { 0, 0 };
	if (pause) {
		pause = false;
		std::cout << "START GAME: Game started!" << std::endl;
	}
}
double Snake::getSpeed() {
	return SnakeConstants::SNAKE_SPEED + foodCounter * SnakeConstants::BOOST_PER_FOOD;
}
int Snake::getFood() {
	return foodCounter;
}
#pragma endregion

#pragma region Private
bool Snake::checkFoodCollision(Food* food) {
	if (Vector2IntEquals(body[0], food->getPosition())) {
		food->findNewPosition(body);
		return true;
	}
	return false;
}
void Snake::checkEdgeCollision() {
	if (body[0].x < SnakeConstants::MARGIN.x) {
		std::cout << "TELEPORT: Snake exited to the left!" << std::endl;
		body[0] = {SnakeConstants::WINDOW_WIDTH / SnakeConstants::CELL_SIZE - (int)SnakeConstants::MARGIN.z - 1, body[0].y};
		PlaySound(wallSound);
	}
	else if (body[0].x >= SnakeConstants::WINDOW_WIDTH / SnakeConstants::CELL_SIZE - SnakeConstants::MARGIN.z) {
		std::cout << "TELEPORT: Snake exited to the right!" << std::endl;
		body[0] = { (int)SnakeConstants::MARGIN.x, body[0].y};
		PlaySound(wallSound);
	}
	else if (body[0].y < SnakeConstants::MARGIN.y) {
		std::cout << "TELEPORT: Snake exited up!" << std::endl;
		body[0] = {body[0].x, SnakeConstants::WINDOW_HEIGHT / SnakeConstants::CELL_SIZE - (int)SnakeConstants::MARGIN.w - 1 };
		PlaySound(wallSound);
	}
	else if (body[0].y >= SnakeConstants::WINDOW_HEIGHT / SnakeConstants::CELL_SIZE - SnakeConstants::MARGIN.w) {
		std::cout << "TELEPORT: Snake exited down!" << std::endl;
		body[0] = {body[0].x, (int)SnakeConstants::MARGIN.y };
		PlaySound(wallSound);
	}
}
void Snake::checkTailCollision(Food* food) {
	for (int i = 1; i < body.size(); i++) {
		if (Vector2IntEquals(body[0], body[i])) {
			PlaySound(gameOverSound);
			food->findNewPosition(body);
			reset();
		}
	}
}
void Snake::reset() {
	int x = (SnakeConstants::WINDOW_WIDTH / 2) / SnakeConstants::CELL_SIZE;
	int y = (SnakeConstants::WINDOW_HEIGHT / 2) / SnakeConstants::CELL_SIZE;
	body = std::deque<Vector2Int>{
		{x,y},
		{x - 1,y},
		{x - 2,y},
	};
	direction = { 1,0 };
	pause = true;
	std::cout << "GAME OVER: Game ended!" << std::endl;
}
#pragma endregion