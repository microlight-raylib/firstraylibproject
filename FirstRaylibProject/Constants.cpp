#include "Constants.h"

namespace SnakeConstants {
	static const double SNAKE_SPEED = 5;
	static const double BOOST_PER_FOOD = 0.2;
	static const int CELL_SIZE = 25;
	static const int WINDOW_WIDTH = 700;
	static const int WINDOW_HEIGHT = 700;
	static const int FONT_SIZE = 40;

	static const Vector4 MARGIN = { 1,3,1,1 };

	static const Color GREEN_COLOR = { 173,204,96,255 };
	static const Color DARKGREEN_COLOR = { 43,51,24,255 };
}

bool Vector2IntEquals(Vector2Int vector1, Vector2Int vector2) {
	if (vector1.x == vector2.x && vector1.y == vector2.y)
		return true;
	return false;
}
Vector2Int Vector2IntAdd(Vector2Int vector1, Vector2Int vector2) {
	return Vector2Int{ vector1.x + vector2.x, vector1.y + vector2.y };
}