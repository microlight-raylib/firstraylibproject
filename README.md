# Raylib Snake

Simple snake game made in Raylib framework using C++ as language.

## Gameplay

![Gameplay](gitData/screenshot.png?raw=true "Gameplay")

## UI

UI system is modular, with few parameters easily change layout of the game UI.